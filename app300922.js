/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const process = require('process');
const http = require('http');
  	const exec = require('child_process').exec;


const { Gateway, Wallets } = require('fabric-network');
const FabricCAServices = require('fabric-ca-client');
const path = require('path');
const { buildCAClient, registerAndEnrollUser, enrollAdmin } = require('../../test-application/javascript/CAUtil.js');
const { buildCCPOrg1, buildWallet } = require('../../test-application/javascript/AppUtil.js');

const channelName = 'mychannel';
const chaincodeName = 'basic';
const mspOrg1 = 'Org1MSP';
const walletPath = path.join(__dirname, 'wallet');
const org1UserId = 'appUser';

function extfun2(a, b) {
	var c = a * b
	return c
  }  
  

function prettyJSONString(inputString) {
	return JSON.stringify(JSON.parse(inputString), null, 2);
}

/*
________________________________________________________________________________

peer chaincode query -C mychannel -n basic -c '{"Args":["getAllAssets"]}'

peer chaincode query -C mychannel -n basic -c '{"Args":["getAllStocks"]}'

peer chaincode query -C mychannel -n basic -c '{"Args":["getAllContrats"]}'

1649855219


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createStock","Args":["1649855219","stockYAM1","100","0"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createStock","Args":["1649855219","stockYAM2","100","0"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createStock","Args":["stockYAM3","100","0"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createContrat","Args":["1649855219","contratYAM1","SANS_COMMANDE","50"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createContrat","Args":["contratYAM2","SANS_COMMANDE","50"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"deleteContrat","Args":["contratYAM2"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"deleteStock","Args":["stockYAM2"]}'



peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readStock","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readStock","Args":["stockYAM2"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readContrat","Args":["contratYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readContrat","Args":["contratYAM2"]}'


peer chaincode query -C mychannel -n basic -c '{"function":"readStock","Args":["stockYAM1"]}'

peer chaincode query -C mychannel -n basic -c '{"function":"readContrat","Args":["contratYAM1"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getContratStatus","Args":["contratYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getContratQuantity","Args":["contratYAM1"]}'



peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAssetStock","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAssetEnLivraison","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAllContratsByStatus","Args":["SANS_COMMANDE"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAllContratsByDate","Args":["1649855218","1649855290"]}'




peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"augmentAssetStock","Args":["stockYAM1","50"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"updateStock","Args":["stockYAM1","200","50"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"checkAvailability","Args":["1649855289","stockYAM1","contratYAM1","30"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"validClient","Args":["1649855289","stockYAM1","contratYAM1","30"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"progressDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"finalizeDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getHistory","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getHistory","Args":["contratYAM1"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getEnv","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getEnv","Args":["contratYAM1"]}'



*/


// pre-requisites:
// - fabric-sample two organization test-network setup with two peers, ordering service,
//   and 2 certificate authorities
//         ===> from directory /fabric-samples/test-network
//         ./network.sh down
//         ./network.sh up createChannel -ca
// - Use any of the asset-transfer-basic chaincodes deployed on the channel "mychannel"
//   with the chaincode name of "basic". The following deploy command will package,
//   install, approve, and commit the javascript chaincode, all the actions it takes
//   to deploy a chaincode to a channel.
//         ===> from directory /fabric-samples/test-network
//         ./network.sh deployCC -ccn basic -ccp ../asset-transfer-basic/chaincode-javascript/ -ccl javascript
// - Be sure that node.js is installed
//         ===> from directory /fabric-samples/asset-transfer-basic/application-javascript
//         node -v
//
//  sudo -i
//  Sc_2021
//  cd /home/ope-sboubaker/fabric/fabric-samples/asset-transfer-basic/application-javascript
//
// - npm installed code dependencies
//         ===> from directory /fabric-samples/asset-transfer-basic/application-javascript
//         npm install
// - to run this test application
//         ===> from directory /fabric-samples/asset-transfer-basic/application-javascript
// cd wallet
// rm admin.id
// rm appUser.id
// cd /home/ope-sboubaker/fabric/fabric-samples/asset-transfer-basic/application-javascript
//         node app.js

// NOTE: If you see  kind an error like these:
/*
    2020-08-07T20:23:17.590Z - error: [DiscoveryService]: send[mychannel] - Channel:mychannel received discovery error:access denied
    ******** FAILED to run the application: Error: DiscoveryService: mychannel error: access denied

   OR

   Failed to register user : Error: fabric-ca request register failed with errors [[ { code: 20, message: 'Authentication failure' } ]]
   ******** FAILED to run the application: Error: Identity not found in wallet: appUser
*/
// Delete the /fabric-samples/asset-transfer-basic/application-javascript/wallet directory
// and retry this application.
//
// The certificate authority must have been restarted and the saved certificates for the
// admin and application user are not valid. Deleting the wallet store will force these to be reset
// with the new certificate authority.
//

/**
 *  A test application to show basic queries operations with any of the asset-transfer-basic chaincodes
 *   -- How to submit a transaction
 *   -- How to query and check the results
 *
 * To see the SDK workings, try setting the logging to show on the console before running
 *        export HFC_LOGGING='{"debug":"console"}'
 */
async function main() {
	try {

		const exec = require('child_process').exec;

  		function puts(error, stdout, stderr) { console.log(stdout) }

  		exec('ls -l', puts);
	
	    console.log(process.env);	
		
        let param = "";
		let fonction = "";
		
		console.log(process.argv);
		process.argv.forEach((val, index) => {
		console.log(`${index}: ${val}`);
		
//		await contract.submitTransaction('createStock','1649855219', 'stockYAM2', '100', '0'); 

		if (index == 2)
		{
			fonction = val;
			param = param+val;
		}		
		if (index > 2)
		{
			param = param+',';
			param = param+val;
		}
		});
		console.log(" ");
		console.log("param : "+param);
				
		// build an in memory object with the network configuration (also known as a connection profile)
		const ccp = buildCCPOrg1();

		// build an instance of the fabric ca services client based on
		// the information in the network configuration
		const caClient = buildCAClient(FabricCAServices, ccp, 'ca.org1.example.com');

		// setup the wallet to hold the credentials of the application user
		const wallet = await buildWallet(Wallets, walletPath);

		// in a real application this would be done on an administrative flow, and only once
		await enrollAdmin(caClient, wallet, mspOrg1);

		// in a real application this would be done only when a new user was required to be added
		// and would be part of an administrative flow
		await registerAndEnrollUser(caClient, wallet, mspOrg1, org1UserId, 'org1.department1');

		// Create a new gateway instance for interacting with the fabric network.
		// In a real application this would be done as the backend server session is setup for
		// a user that has been verified.
		const gateway = new Gateway();

		try {
			// setup the gateway instance
			// The user will now be able to create connections to the fabric network and be able to
			// submit transactions and query. All transactions submitted by this gateway will be
			// signed by this user using the credentials stored in the wallet.
			await gateway.connect(ccp, {
				wallet,
				identity: org1UserId,
				discovery: { enabled: true, asLocalhost: true } // using asLocalhost as this gateway is using a fabric network deployed locally
			});

			// Build a network instance based on the channel where the smart contract is deployed
			const network = await gateway.getNetwork(channelName);

			// Get the contract from the network.
			const contract = network.getContract(chaincodeName);

			// Initialize a set of asset data on the channel using the chaincode 'InitLedger' function.
			// This type of transaction would only be run once by an application the first time it was started after it
			// deployed the first time. Any updates to the chaincode deployed later would likely not need to run
			// an "init" type function.
			
			
			console.log('<br/><br/>--> Submit Transaction: InitLedger, function creates the initial set of assets on the ledger');
			await contract.submitTransaction('InitLedger');
			console.log("<br/><br/>"+'*** Result: committed');

			// Let's try a query type operation (function).
			// This will be sent to just one peer and the results will be shown.
			console.log('<br/><br/>--> Evaluate Transaction: getAllAssets, function returns all the current assets on the ledger');
			let result = await contract.evaluateTransaction('getAllAssets');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);						


//--------------------------------------------------
/*	
			console.log('<br/><br/>--> Submit Transaction: deleteStock');
			result = await contract.submitTransaction('deleteStock','stockYAM2');
			console.log('*** Result: committed');
			if (`${result}` !== '') {
				console.log(`*** Result: ${prettyJSONString(result.toString())}`);
			}
*/						
//--------------------------------------------------
//'{"function":"createStock","Args":["1649855219","stockYAM2","100","0"]}'
		
			console.log('<br/><br/>--> Submit Transaction: createStock');
			result = await contract.submitTransaction('createStock','1649855219', 'stockYAM2', '100', '0'); 
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>");
				console.log(`*** Result: ${prettyJSONString(result.toString())}`);
			}
					
//--------------------------------------------------
			
			console.log('<br/><br/>--> Evaluate Transaction: readStock');
			result = await contract.evaluateTransaction('readStock','stockYAM2');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			
//--------------------------------------------------
			
			console.log('<br/><br/>--> Evaluate Transaction: getAllStocks, function returns all the current Stocks on the ledger');
			result = await contract.evaluateTransaction('getAllStocks');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
					
//--------------------------------------------------
//'{"function":"getAssetStock","Args":["stockYAM2"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getAssetStock');
			result = await contract.evaluateTransaction('getAssetStock','stockYAM2');
//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);		
			console.log("<br/><br/>"+`*** Result: ${result}`);				
		
//--------------------------------------------------
//'{"function":"getAssetEnLivraison","Args":["stockYAM2"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getAssetEnLivraison');
			result = await contract.evaluateTransaction('getAssetEnLivraison','stockYAM2');
//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);		
			console.log("<br/><br/>"+`*** Result: ${result}`);				
							
//--------------------------------------------------
//'{"function":"augmentAssetStock","Args":["1649855219","stockYAM2","50"]}'
		
			console.log('<br/><br/>--> Submit Transaction: augmentAssetStock');
			result = await contract.submitTransaction('augmentAssetStock','1649855219','stockYAM2','50');
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}
			
//--------------------------------------------------
//'{"function":"updateStock","Args":["stockYAM2","200","50"]}'
		
			console.log('<br/><br/>--> Submit Transaction: updateStock');
			result = await contract.submitTransaction('updateStock','stockYAM2','200','50');
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}
		
			
//--------------------------------------------------
/*
  		console.log('<br/><br/>--> Submit Transaction: deleteContrat');
 			result = await contract.submitTransaction('deleteContrat','contratYAM1');
 			console.log('*** Result: committed');
 			if (`${result}` !== '') {
 				console.log(`*** Result: ${prettyJSONString(result.toString())}`);
 			}
*/			
//--------------------------------------------------
// '{"function":"createContrat","Args":["1649855219","contratYAM1","SANS_COMMANDE","50"]}'
		
			console.log('<br/><br/>--> Submit Transaction: createContrat');
			result = await contract.submitTransaction('createContrat',"1649855219", 'contratYAM1', 'SANS_COMMANDE', '50');
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}

//--------------------------------------------------
		
			console.log('<br/><br/>--> Evaluate Transaction: readContrat');
			result = await contract.evaluateTransaction('readContrat','contratYAM1');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			
//--------------------------------------------------
				
			console.log('<br/><br/>--> Evaluate Transaction: getAllContrats, function returns all the current Contrats on the ledger');
			result = await contract.evaluateTransaction('getAllContrats');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			
//--------------------------------------------------
// '{"function":"getContratStatus","Args":["contratYAM1"]}'
				
			console.log('<br/><br/>--> Evaluate Transaction: getContratStatus');
			result = await contract.evaluateTransaction('getContratStatus','contratYAM1');
			console.log("<br/><br/>"+`*** Result: ${result}`);				
			
//			console.log(`*** Result: ${prettyJSONString(result1.toString())}`);		
		
//--------------------------------------------------
//'{"function":"getContratQuantity","Args":["contratYAM1"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getContratQuantity');
			result = await contract.evaluateTransaction('getContratQuantity','contratYAM1');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);		
		
//--------------------------------------------------
//'{"function":"getAllContratsByStatus","Args":["SANS_COMMANDE"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getAllContratsByStatus');
			result = await contract.evaluateTransaction('getAllContratsByStatus','SANS_COMMANDE');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);		
		
//--------------------------------------------------
//'{"function":"getAllContratsByDate","Args":["1649855218","1649855290"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getAllContratsByDate');
			result = await contract.evaluateTransaction('getAllContratsByDate','1649855218','1649855290');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);	


//--------------------------------------------------
//'{"function":"checkAvailability","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
		
			console.log('<br/><br/>--> Submit Transaction: checkAvailability');
			result = await contract.submitTransaction('checkAvailability',"1649855289",'stockYAM2',"contratYAM1","30");
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${result}`);				
				
//				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}

//--------------------------------------------------					
//'{"function":"validClient","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
	
			console.log('<br/><br/>--> Submit Transaction: validClient');
			result = await contract.submitTransaction('validClient',"1649855289",'stockYAM2',"contratYAM1","30");
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${result}`);				
				
//				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}
		
//--------------------------------------------------
//'{"function":"progressDelivery","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
	
			console.log('<br/><br/>--> Submit Transaction: progressDelivery');
			result = await contract.submitTransaction('progressDelivery',"1649855289",'stockYAM2',"contratYAM1","30");
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${result}`);				
				
//				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}
	
//--------------------------------------------------
//'{"function":"finalizeDelivery","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
	
			console.log('<br/><br/>--> Submit Transaction: finalizeDelivery');
			result = await contract.submitTransaction('finalizeDelivery',"1649855289",'stockYAM2',"contratYAM1","30");
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${result}`);				
				
//				console.log(`*** Result: ${prettyJSONString(result.toString())}`);
			}
	
//--------------------------------------------------
//'{"function":"getHistory","Args":["stockYAM2"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getHistory STOCK');
			result = await contract.evaluateTransaction('getHistory','stockYAM2');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);	

//--------------------------------------------------
//'{"function":"getHistory","Args":["contratYAM1"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getHistory CONTRAT');
			result = await contract.evaluateTransaction('getHistory','contratYAM1');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);	

//--------------------------------------------------
//'{"function":"getEnv","Args":["stockYAM2"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getEnv STOCK');
			result = await contract.evaluateTransaction('getEnv','stockYAM2');
//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);	
			
			console.log("<br/><br/>"+'*** Result: committed');
			
//--------------------------------------------------
//'{"function":"getEnv","Args":["contratYAM1"]}'
			
			console.log('<br/><br/>--> Evaluate Transaction: getEnv CONTRAT');
			result = await contract.evaluateTransaction('getEnv','contratYAM1');
//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);	
			
			console.log("<br/><br/>"+'*** Result: committed');

//--------------------------------------------------
//--------------------------------------------------
	
			console.log('<br/><br/>--> Submit Transaction: deleteStock');
			result = await contract.submitTransaction('deleteStock','stockYAM2');
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}
			
//--------------------------------------------------

			console.log('<br/><br/>--> Submit Transaction: deleteContrat');
			result = await contract.submitTransaction('deleteContrat','contratYAM1');
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}
					
//--------------------------------------------------

const serveur = http.createServer((requete,reponse) =>  { 
  console.log(requete.method);
  console.log(requete.url);
  reponse.setHeader("content-type","text/html");
  reponse.write("<head><meta charset='utf8'><head>")
  reponse.write("<h1>SUPPLYCHAIN</h1><p>hello  </p>");
   
//--------------------------------------------------
//'{"function":"createStock","Args":["1649855219","stockYAM2","100","0"]}'
		
console.log('<br/><br/>--> Submit Transaction: createStock');
//result = await contract.submitTransaction('createStock','1649855219', 'stockYAM2', '100', '0'); 
//result =  contract.submitTransaction('createStock','1649855219', 'stockYAM2', '100', '0'); 
reponse.write('<br/><br/>--> Submit Transaction: createStock');
reponse.write(`*** Result: ${prettyJSONString(result.toString())}`);

/*
console.log("<br/><br/>"+'*** Result: committed');
if (`${result}` !== '') {
	console.log("<br/><br/>");
	console.log(`*** Result: ${prettyJSONString(result.toString())}`);
}
reponse.write(`*** Result: ${prettyJSONString(result.toString())}`);
*/		
//--------------------------------------------------

  reponse.end();
});

serveur.listen(3001, "localhost",() => {
  console.log("Attente des requêtes au port 3001")
})


/* yam1 

			// Now let's try to submit a transaction.
			// This will be sent to both peers and if both peers endorse the transaction, the endorsed proposal will be sent
			// to the orderer to be committed by each of the peer's to the channel ledger.
			console.log('<br/><br/>--> Submit Transaction: CreateAsset, creates new asset with ID, color, owner, size, and appraisedValue arguments');
			result = await contract.submitTransaction('CreateAsset', 'asset24', 'yellow', '5', 'Tom', '1300');
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}

//			console.log('<br/><br/>--> Evaluate Transaction: ReadAsset, function returns an asset with a given assetID');
//			result = await contract.evaluateTransaction('ReadAsset', 'asset21');
//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);

			console.log('<br/><br/>--> Evaluate Transaction: AssetExists, function returns "true" if an asset with given assetID exist');
			result = await contract.evaluateTransaction('AssetExists', 'asset1');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);

			console.log('<br/><br/>--> Submit Transaction: UpdateAsset asset1, change the appraisedValue to 350');
			await contract.submitTransaction('UpdateAsset', 'asset1', 'blue', '5', 'Tomoko', '350');
			console.log("<br/><br/>"+'*** Result: committed');

			console.log('<br/><br/>--> Evaluate Transaction: ReadAsset, function returns "asset1" attributes');
			result = await contract.evaluateTransaction('ReadAsset', 'asset1');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			
///////////////////////////////////////
//	deleteAsset	
	
			console.log('<br/><br/>--> Submit Transaction: deleteAsset, deletes asset with ID');
			result = await contract.submitTransaction('deleteAsset', 'asset24');
			console.log("<br/><br/>"+'*** Result: committed');
			if (`${result}` !== '') {
				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			}	
	
///////////////////////////////////////


			try {
				// How about we try a transactions where the executing chaincode throws an error
				// Notice how the submitTransaction will throw an error containing the error thrown by the chaincode
				console.log('<br/><br/>--> Submit Transaction: UpdateAsset asset70, asset70 does not exist and should return an error');
				await contract.submitTransaction('UpdateAsset', 'asset70', 'blue', '5', 'Tomoko', '300');
				console.log("<br/><br/>"+'******** FAILED to return an error');
			} catch (error) {
				console.log("<br/><br/>"+`*** Successfully caught the error: <br/><br/>    ${error}`);
			}

			console.log('<br/><br/>--> Submit Transaction: TransferAsset asset1, transfer to new owner of Tom');
			await contract.submitTransaction('TransferAsset', 'asset1', 'Tom');
			console.log("<br/><br/>"+'*** Result: committed');

			console.log('<br/><br/>--> Evaluate Transaction: ReadAsset, function returns "asset1" attributes');
			result = await contract.evaluateTransaction('ReadAsset', 'asset1');
			console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);

 yam1   */  

			
		} finally {
			//alert('HELLO YAYA3');
			// Disconnect from the gateway when the application is closing
			// This will close all connections to the network
			gateway.disconnect();
		}
	} catch (error) {
		console.error("<br/><br/>"+`******** FAILED to run the application: ${error}`);
	}
}

main();
