/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';
let newstr = "";
let val = "";
let repok = "OK";
let resaffich = "";
let affichres = "";
let listeres = "";
let datecour;
let hcour;
let result2;
let result1;
let reponse1;

let nbreq = 0;
let contract;
let ccp;
let wallet;
let result;

const process = require('process');
const http = require('http');
const exec = require('child_process').exec;
const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const bodyParser = require('body-parser');
const app = express();

//middlewares

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser('secret'));
app.use(session({ cookie: { maxAge: null } }));

//flash message middleware
app.use((req, res, next) => {
	res.locals.message = req.session.message
	delete req.session.message
	next()
})

const handlebars = require('express3-handlebars').create();
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

const { Gateway, Wallets } = require('fabric-network');
const FabricCAServices = require('fabric-ca-client');
const path = require('path');
const { buildCAClient, registerAndEnrollUser, enrollAdmin } = require('../../test-application/javascript/CAUtil.js');
const { buildCCPOrg1, buildWallet } = require('../../test-application/javascript/AppUtil.js');

const channelName = 'mychannel';
const chaincodeName = 'basic';
const mspOrg1 = 'Org1MSP';
const walletPath = path.join(__dirname, 'wallet');
const org1UserId = 'appUser';
/*
setTimeout(() => {
	console.log("dlStock Retarder de 10 secondes.");
  }, "10000")
*/

//--------------------------------------------------
function printHeure() {
	datecour = new Date();
	hcour = datecour.getHours() + ":" + datecour.getMinutes() + ":" + datecour.getSeconds();
	console.log(hcour);
}
//--------------------------------------------------
/*
async function puts(error, stdout, stderr) {
	affichres=stdout+stderr;
	console.log("puts affichres: "+affichres);
	console.log("stdout: "+stdout);
	console.log("stderr: "+stderr);
	if (error !== null) {
		console.log(`exec error: ${error}`);
	return affichres
	}
}
*/
//--------------------------------------------------
function putw(error, stdout, stderr) {
	//console.log(stdout);
	//console.log(stderr);
	affichres = stdout + stderr;
	affichres = affichres.toString();
	console.log("affichres : " + affichres);
	//	listeres=listeres+('<br/><br/>*** Result SHELL : <br/><br/>'+affichres);
	//	console.log("listeres : "+listeres);

	if (error !== null) {
		console.log(`exec error: ${error}`);
		listeres = listeres + ('<br/><br/>exec error:' + error);
	}
}
//--------------------------------------------------
function prettyJSONString(inputString) {
	return JSON.stringify(JSON.parse(inputString), null, 2);
}
//--------------------------------------------------
async function GAllAssets() {
	try {
		console.log('<br/><br/>--> Evaluate Transaction: getAllAssets, function returns all the current assets on the ledger');
		result = await contract.evaluateTransaction('getAllAssets');
	} catch (error) {
		console.error("<br/><br/>" + `******** FAILED GAllAssets: ${error}`);
	}
	setTimeout(() => {
		console.log("GAllAssets Retarder de 10 secondes.");
	}, "10000")
	console.log("<br/><br/>" + `*** Result: ${result}`);
	console.log("<br/><br/>" + `FIN GAllAssets `);
	return result;
}
//--------------------------------------------------
async function crStock() {
	result1 = 1;
	try {
		console.log('--> Submit Transaction: createStock');
		//		listeres=listeres+('<br/><br/>--> Submit Transaction: createStock');
		//result1 = await contract.submitTransaction('createStock','1649855219', 'stockYAM2', '100', '0'); 

		let truc = async () => {
			result1 = await contract.submitTransaction('createStock', '1649855219', 'stockYAM2', '100', '0');
			return result1;
		};
		truc().then((value) => {
			console.log("value : " + value);
			result1 = value;
		});

		//		console.log("<br/><br/>"+'*** Retour submit');
	} catch (error) {
		console.error(`******** FAILED crStock: ${error}`);
	}

	console.log(`*** Result crStock: ${prettyJSONString(result1.toString())}`);
	printHeure();
	listeres = ('*** Result crStock : ' + ` : ${prettyJSONString(result1.toString())}`);
	//reponse1.write(listeres);
	printHeure();
	console.log(`FIN crStock `);
	//reponse1.end();
	return result1;
}
//--------------------------------------------------
async function lireStock() {
	result1 = 1;
	try {
		console.log('<br/><br/>--> Evaluate Transaction: readStock');
		listeres = listeres + ('<br/><br/>--> Submit Transaction: readStock');
		result1 = await contract.evaluateTransaction('readStock', 'stockYAM2');
		//		console.log("<br/><br/>"+'*** Retour submit');
	} catch (error) {
		console.error("<br/><br/>" + `******** FAILED readStock: ${error}`);
	}
	console.log(`*** Result: ${prettyJSONString(result1.toString())}`);
	printHeure();
	listeres = listeres + ("<br/><br/>" + '*** Result REQUETE : ' + nbreq + ` : ${prettyJSONString(result1.toString())}`);
	reponse1.write(listeres);
	printHeure();
	console.log("<br/><br/>" + `FIN lireStock `);
	reponse1.end();
	return result1;
}
//--------------------------------------------------
async function dlStock() {
	result1 = 1;
	try {
		console.log('<br/><br/>--> Submit Transaction: deleteStock');
		listeres = listeres + ('<br/><br/>--> Submit Transaction: deleteStock');
		result1 = await contract.submitTransaction('deleteStock', 'stockYAM2');
		//		console.log("<br/><br/>"+'*** Retour submit');
	} catch (error) {
		console.error("<br/><br/>" + `******** FAILED dlStock: ${error}`);
	}
	console.log(`*** Result: ${prettyJSONString(result1.toString())}`);
	printHeure();
	listeres = listeres + ("<br/><br/>" + '*** Result REQUETE : ' + nbreq + ` : ${prettyJSONString(result1.toString())}`);
	//	reponse1.write(listeres);
	printHeure();
	console.log("<br/><br/>" + `FIN dlStock `);
	reponse1.end();
	return result1;
}
//--------------------------------------------------
/*
________________________________________________________________________________

peer chaincode query -C mychannel -n basic -c '{"Args":["getAllAssets"]}'

peer chaincode query -C mychannel -n basic -c '{"Args":["getAllStocks"]}'

peer chaincode query -C mychannel -n basic -c '{"Args":["getAllContrats"]}'

1649855219


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createStock","Args":["1649855219","stockYAM1","100","0"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createStock","Args":["1649855219","stockYAM2","100","0"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createStock","Args":["stockYAM3","100","0"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createContrat","Args":["1649855219","contratYAM1","SANS_COMMANDE","50"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"createContrat","Args":["contratYAM2","SANS_COMMANDE","50"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"deleteContrat","Args":["contratYAM2"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"deleteStock","Args":["stockYAM2"]}'



peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readStock","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readStock","Args":["stockYAM2"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readContrat","Args":["contratYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"readContrat","Args":["contratYAM2"]}'


peer chaincode query -C mychannel -n basic -c '{"function":"readStock","Args":["stockYAM1"]}'

peer chaincode query -C mychannel -n basic -c '{"function":"readContrat","Args":["contratYAM1"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getContratStatus","Args":["contratYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getContratQuantity","Args":["contratYAM1"]}'



peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAssetStock","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAssetEnLivraison","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAllContratsByStatus","Args":["SANS_COMMANDE"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getAllContratsByDate","Args":["1649855218","1649855290"]}'




peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"augmentAssetStock","Args":["stockYAM1","50"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"updateStock","Args":["stockYAM1","200","50"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"checkAvailability","Args":["1649855289","stockYAM1","contratYAM1","30"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"validClient","Args":["1649855289","stockYAM1","contratYAM1","30"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"progressDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"finalizeDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getHistory","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getHistory","Args":["contratYAM1"]}'


peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getEnv","Args":["stockYAM1"]}'

peer chaincode invoke $HOST_ORDERER $CHANNEL $CHAINCODE ${PEER_Client} ${PEER_Fournisseur} -c '{"function":"getEnv","Args":["contratYAM1"]}'



*/


// pre-requisites:
// - fabric-sample two organization test-network setup with two peers, ordering service,
//   and 2 certificate authorities
//         ===> from directory /fabric-samples/test-network
//         ./network.sh down
//         ./network.sh up createChannel -ca
// - Use any of the asset-transfer-basic chaincodes deployed on the channel "mychannel"
//   with the chaincode name of "basic". The following deploy command will package,
//   install, approve, and commit the javascript chaincode, all the actions it takes
//   to deploy a chaincode to a channel.
//         ===> from directory /fabric-samples/test-network
//         ./network.sh deployCC -ccn basic -ccp ../asset-transfer-basic/chaincode-javascript/ -ccl javascript
// - Be sure that node.js is installed
//         ===> from directory /fabric-samples/asset-transfer-basic/application-javascript
//         node -v
//
//  sudo -i
//  Sc_2021
//  cd /home/ope-sboubaker/fabric/fabric-samples/asset-transfer-basic/application-javascript
//
// - npm installed code dependencies
//         ===> from directory /fabric-samples/asset-transfer-basic/application-javascript
//         npm install
// - to run this test application
//         ===> from directory /fabric-samples/asset-transfer-basic/application-javascript
// cd wallet
// rm admin.id
// rm appUser.id
// cd /home/ope-sboubaker/fabric/fabric-samples/asset-transfer-basic/application-javascript
//         node app.js

// NOTE: If you see  kind an error like these:
/*
	2020-08-07T20:23:17.590Z - error: [DiscoveryService]: send[mychannel] - Channel:mychannel received discovery error:access denied
	******** FAILED to run the application: Error: DiscoveryService: mychannel error: access denied

   OR

   Failed to register user : Error: fabric-ca request register failed with errors [[ { code: 20, message: 'Authentication failure' } ]]
   ******** FAILED to run the application: Error: Identity not found in wallet: appUser
*/
// Delete the /fabric-samples/asset-transfer-basic/application-javascript/wallet directory
// and retry this application.
//
// The certificate authority must have been restarted and the saved certificates for the
// admin and application user are not valid. Deleting the wallet store will force these to be reset
// with the new certificate authority.
//

/**
 *  A test application to show basic queries operations with any of the asset-transfer-basic chaincodes
 *   -- How to submit a transaction
 *   -- How to query and check the results
 *
 * To see the SDK workings, try setting the logging to show on the console before running
 *        export HFC_LOGGING='{"debug":"console"}'
 */
async function main() {

	try {
		//----------------------------------------------------------------------------------
		app.get('/essai', (req, res) => {
			console.log('---> GET /essai');

			res.render('essai');
		});
		//----------------------------------------------------------------------------------
		app.get('/TestNetwork', (req, res) => {
			res.render('TestNetwork');
		});
		//----------------------------------------------------------------------------------
		app.get('/', (req, res) => {
			res.render('accueil');
		});
		//----------------------------------------------------------------------------------
		app.get('/createStock', (req, res) => {
			res.render('createStock');
		});
		//----------------------------------------------------------------------------------
		app.get('/readStock', (req, res) => {
			res.render('readStock');
		});
		//----------------------------------------------------------------------------------
		app.get('/getAllStocks', (req, res) => {
			console.log('---> GET /getAllStocks');

			res.render('getAllStocks');
		});
		//----------------------------------------------------------------------------------
		app.get('/getAssetStock', (req, res) => {
			res.render('getAssetStock');
		});
		//----------------------------------------------------------------------------------
		app.get('/getAssetEnLivraison', (req, res) => {
			res.render('getAssetEnLivraison');
		});
		//----------------------------------------------------------------------------------
		app.get('/augmentAssetStock', (req, res) => {
			res.render('augmentAssetStock');
		});
		//----------------------------------------------------------------------------------
		app.get('/updateStock', (req, res) => {
			res.render('updateStock');
		});
		//----------------------------------------------------------------------------------
		app.get('/createContrat', (req, res) => {
			res.render('createContrat');
		});
		//----------------------------------------------------------------------------------
		app.get('/readContrat', (req, res) => {
			res.render('readContrat');
		});
		//----------------------------------------------------------------------------------
		app.get('/getAllContrats', (req, res) => {
			res.render('getAllContrats');
		});
		//----------------------------------------------------------------------------------
		app.get('/getContratStatus', (req, res) => {
			res.render('getContratStatus');
		});
		//----------------------------------------------------------------------------------
		app.get('/getContratQuantity', (req, res) => {
			res.render('getContratQuantity');
		});
		//----------------------------------------------------------------------------------
		app.get('/getAllContratsByStatus', (req, res) => {
			res.render('getAllContratsByStatus');
		});
		//----------------------------------------------------------------------------------
		app.get('/getAllContratsByDate', (req, res) => {
			res.render('getAllContratsByDate');
		});
		//----------------------------------------------------------------------------------
		app.get('/checkAvailability', (req, res) => {
			res.render('checkAvailability');
		});
		//----------------------------------------------------------------------------------
		app.get('/validClient', (req, res) => {
			res.render('validClient');
		});
		//----------------------------------------------------------------------------------
		app.get('/progressDelivery', (req, res) => {
			res.render('progressDelivery');
		});
		//----------------------------------------------------------------------------------
		app.get('/finalizeDelivery', (req, res) => {
			res.render('finalizeDelivery');
		});
		//----------------------------------------------------------------------------------
		app.get('/getHistory', (req, res) => {
			res.render('getHistory');
		});
		//----------------------------------------------------------------------------------
		app.get('/getEnv', (req, res) => {
			res.render('getEnv');
		});
		//----------------------------------------------------------------------------------
		app.get('/deleteStock', (req, res) => {
			res.render('deleteStock');
		});
		//----------------------------------------------------------------------------------
		app.get('/deleteContrat', (req, res) => {
			res.render('deleteContrat');
		});
		//----------------------------------------------------------------------------------
		app.post('/createStock', (req, res) => {
			if (req.body.stockID == '' || req.body.stock == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/createStock')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: createStock');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							console.log('createStock dateconv: ' + dateconv);
							result = await contract.submitTransaction('createStock', now, req.body.stockID, req.body.stock, 0);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('createStock', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'createStock ',
							message: val
						}
						*/
						res.render('createStock', { resul: val })
						//res.redirect('/createStock')
					});
				} catch (error) {
					console.error(`******** FAILED createStock: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID, req.body.stock);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/updateStock', (req, res) => {
			if (req.body.stockID == '' || req.body.stock == '' || req.body.enLivraison == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/updateStock')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: updateStock');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							console.log('updateStock dateconv: ' + dateconv);
							result = await contract.submitTransaction('updateStock', req.body.stockID, req.body.stock, req.body.enLivraison);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('updateStock', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'updateStock ',
							message: val
						}
						*/
						res.render('updateStock', { resul: val })
						//res.redirect('/updateStock')
					});
				} catch (error) {
					console.error(`******** FAILED updateStock: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID, req.body.stock, req.body.enLivraison);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/augmentAssetStock', (req, res) => {
			if (req.body.stockID == '' || req.body.stock == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/augmentAssetStock')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: augmentAssetStock');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							result = await contract.submitTransaction('augmentAssetStock', now, req.body.stockID, req.body.stock);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('augmentAssetStock', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'augmentAssetStock ',
							message: val
						}
						*/
						res.render('augmentAssetStock', { resul: val })
						//res.redirect('/augmentAssetStock')
					});
				} catch (error) {
					console.error(`******** FAILED augmentAssetStock: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID, req.body.stock);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/readStock', (req, res) => {
			if (req.body.stockID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/readStock')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: readStock');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('readStock', req.body.stockID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('readStock', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'deleteStock ',
							message: val
						}
						*/
						res.render('readStock', { resul: val })
						//res.redirect('/readStock')
					});
				} catch (error) {
					console.error(`******** FAILED readStock: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getHistory', (req, res) => {
			if (req.body.ID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getHistory')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getHistory');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getHistory', req.body.ID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getHistory', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getHistory ',
							message: val
						}
						*/
						res.render('getHistory', { resul: val })
						//res.redirect('/getHistory')
					});
				} catch (error) {
					console.error(`******** FAILED getHistory: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.ID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getEnv', (req, res) => {
			if (req.body.ID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getEnv')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getEnv');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getEnv', req.body.ID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getEnv', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getEnv ',
							message: val
						}
						*/
						res.render('getEnv', { resul: val })
						//res.redirect('/getEnv')
					});
				} catch (error) {
					console.error(`******** FAILED getEnv: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.ID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getAssetEnLivraison', (req, res) => {
			if (req.body.stockID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getAssetEnLivraison')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getAssetEnLivraison');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getAssetEnLivraison', req.body.stockID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getAssetEnLivraison', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getAssetEnLivraison ',
							message: val
						}
						*/
						res.render('getAssetEnLivraison', { resul: val })
						//res.redirect('/getAssetEnLivraison')
					});
				} catch (error) {
					console.error(`******** FAILED getAssetEnLivraison: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getAssetStock', (req, res) => {
			if (req.body.stockID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getAssetStock')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getAssetStock');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getAssetStock', req.body.stockID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getAssetStock', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getAssetStock ',
							message: val
						}
						*/
						res.render('getAssetStock', { resul: val })
						//res.redirect('/getAssetStock')
					});
				} catch (error) {
					console.error(`******** FAILED getAssetStock: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/deleteStock', (req, res) => {
			if (req.body.stockID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/deleteStock')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: deleteStock');
					let truc = async () => {
						try {
							result = await contract.submitTransaction('deleteStock', req.body.stockID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('deleteStock', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'deleteStock ',
							message: val
						}
						*/
						res.render('deleteStock', { resul: val })
						//res.redirect('/deleteStock')
					});
				} catch (error) {
					console.error(`******** FAILED deleteStock: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getAllStocks', (req, res) => {
			console.log('---> POST /getAllStocks');
			if (0 == 1) {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getAllStocks')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getAllStocks');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getAllStocks');
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getAllStocks', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value;
						var valjson = JSON.parse(val);
						console.log("*** Resultat getAllStocks: " + val);
						let vallentgh = valjson.length;
						console.log("*** vallentgh : " + vallentgh);
						res.render('getAllStocksREP', { list: valjson });
						//res.redirect('/getAllStocks')
					});
				} catch (error) {
					console.error(`******** FAILED getAllStocks: ${error}`);
				}
				//--------------------------------------	  
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/createContrat', (req, res) => {
			if (req.body.contratID == '' || req.body.statut == '' || req.body.quantite == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/createContrat')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: createContrat');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							console.log('createContrat dateconv: ' + dateconv);
							result = await contract.submitTransaction('createContrat', now, req.body.contratID, req.body.statut, req.body.quantite);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('createContrat', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'createContrat ',
							message: val
						}
						*/
						res.render('createContrat', { resul: val })
						//res.redirect('/createContrat')
					});
				} catch (error) {
					console.error(`******** FAILED createContrat: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.contratID, req.body.statut, req.body.quantite);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/readContrat', (req, res) => {
			if (req.body.contratID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/readContrat')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: readContrat');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('readContrat', req.body.contratID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('readContrat', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'readContrat ',
							message: val
						}
						*/
						res.render('readContrat', { resul: val })
						//res.redirect('/readContrat')
					});
				} catch (error) {
					console.error(`******** FAILED readContrat: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.contratID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/deleteContrat', (req, res) => {
			if (req.body.contratID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/deleteContrat')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: deleteContrat');
					let truc = async () => {
						try {
							result = await contract.submitTransaction('deleteContrat', req.body.contratID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('deleteContrat', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'deleteContrat ',
							message: val
						}
						*/
						res.render('deleteContrat', { resul: val })
						//res.redirect('/deleteContrat')
					});
				} catch (error) {
					console.error(`******** FAILED deleteContrat: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.contratID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getContratStatus', (req, res) => {
			if (req.body.contratID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getContratStatus')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getContratStatus');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getContratStatus', req.body.contratID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getContratStatus', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getContratStatus ',
							message: val
						}
						*/
						res.render('getContratStatus', { resul: val })
						//res.redirect('/readStock')
					});
				} catch (error) {
					console.error(`******** FAILED getContratStatus: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.contratID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getContratQuantity', (req, res) => {
			if (req.body.contratID == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getContratQuantity')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getContratQuantity');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getContratQuantity', req.body.contratID);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getContratQuantity', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getContratQuantity ',
							message: val
						}
						*/
						res.render('getContratQuantity', { resul: val })
						//res.redirect('/getContratQuantity')
					});
				} catch (error) {
					console.error(`******** FAILED getContratQuantity: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.contratID);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getAllContratsByStatus', (req, res) => {
			if (req.body.statut == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getAllContratsByStatus')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getAllContratsByStatus');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getAllContratsByStatus', req.body.statut);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getAllContratsByStatus', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getAllContratsByStatus ',
							message: val
						}
						*/
						res.render('getAllContratsByStatus', { resul: val })
						//res.redirect('/getAllContratsByStatus')
					});
				} catch (error) {
					console.error(`******** FAILED getAllContratsByStatus: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.statut);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getAllContratsByDate', (req, res) => {
			if (req.body.startDate == '' || req.body.endDate == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getAllContratsByDate')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getAllContratsByDate');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getAllContratsByDate', req.body.startDate, req.body.endDate);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getAllContratsByDate', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'getAllContratsByDate ',
							message: val
						}
						*/
						res.render('getAllContratsByDate', { resul: val })
						//res.redirect('/getAllContratsByDate')
					});
				} catch (error) {
					console.error(`******** FAILED getAllContratsByDate: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.startDate, req.body.endDate);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/getAllContrats', (req, res) => {
			if (0 == 1) {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/getAllContrats')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: getAllContrats');
					let truc = async () => {
						try {
							result = await contract.evaluateTransaction('getAllContrats');
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('getAllContrats', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value;
						var valjson = JSON.parse(val);
						console.log("*** Resultat getAllContrats: " + val);
						let vallentgh = valjson.length;
						console.log("*** vallentgh : " + vallentgh);
						res.render('getAllContratsREP', { list: valjson });
						//res.redirect('/getAllContrats')
					});
				} catch (error) {
					console.error(`******** FAILED getAllContrats: ${error}`);
				}
				//--------------------------------------	  
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/checkAvailability', (req, res) => {
			if (req.body.stockID == '' || req.body.contratID == '' || req.body.quantite == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/checkAvailability')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: checkAvailability');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							console.log('checkAvailability dateconv: ' + dateconv);
							result = await contract.submitTransaction('checkAvailability', now, req.body.stockID, req.body.contratID, req.body.quantite);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('checkAvailability', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'checkAvailability ',
							message: val
						}
						*/
						res.render('checkAvailability', { resul: val })
						//res.redirect('/checkAvailability')
					});
				} catch (error) {
					console.error(`******** FAILED checkAvailability: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID, req.body.contratID, req.body.quantite);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/validClient', (req, res) => {
			if (req.body.stockID == '' || req.body.contratID == '' || req.body.quantite == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/validClient')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: validClient');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							console.log('validClient dateconv: ' + dateconv);
							result = await contract.submitTransaction('validClient', now, req.body.stockID, req.body.contratID, req.body.quantite);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('validClient', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'validClient ',
							message: val
						}
						*/
						res.render('validClient', { resul: val })
						//res.redirect('/validClient')
					});
				} catch (error) {
					console.error(`******** FAILED validClient: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID, req.body.contratID, req.body.quantite);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/progressDelivery', (req, res) => {
			if (req.body.stockID == '' || req.body.contratID == '' || req.body.quantite == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/progressDelivery')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: progressDelivery');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							console.log('progressDelivery dateconv: ' + dateconv);
							result = await contract.submitTransaction('progressDelivery', now, req.body.stockID, req.body.contratID, req.body.quantite);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('progressDelivery', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'progressDelivery ',
							message: val
						}
						*/
						res.render('progressDelivery', { resul: val })
						//res.redirect('/progressDelivery')
					});
				} catch (error) {
					console.error(`******** FAILED progressDelivery: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID, req.body.contratID, req.body.quantite);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/finalizeDelivery', (req, res) => {
			if (req.body.stockID == '' || req.body.contratID == '' || req.body.quantite == '') {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/finalizeDelivery')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: finalizeDelivery');
					let truc = async () => {
						try {
							const now = Date.now();
							console.log('date now: ' + now);
							const date = '1649855219';
							const milliseconds = (date) * 1000;
							const dateconv = new Date(milliseconds);
							console.log('finalizeDelivery dateconv: ' + dateconv);
							result = await contract.submitTransaction('finalizeDelivery', now, req.body.stockID, req.body.contratID, req.body.quantite);
						} catch (error) {
							console.error(`******** FAILED submitTransaction: ${error}`);
							res.render('finalizeDelivery', { resul: error })
						}
						return result;
					};
					truc().then((value) => {
						val = value.toString();
						console.log("*** Resultat : " + val);
						/*
						req.session.message = {
							type: 'success',
							intro: 'finalizeDelivery ',
							message: val
						}
						*/
						res.render('finalizeDelivery', { resul: val })
						//res.redirect('/finalizeDelivery')
					});
				} catch (error) {
					console.error(`******** FAILED finalizeDelivery: ${error}`);
				}
				//--------------------------------------	  
				console.log(req.body.stockID, req.body.contratID, req.body.quantite);
			}
		})
		//----------------------------------------------------------------------------------
		app.post('/TestNetwork', (req, res) => {
			if (0 == 1) {
				req.session.message = {
					type: 'danger',
					intro: 'Empty fields! ',
					message: 'Please insert the requested information.'
				}
				res.redirect('/TestNetwork')
			}
			else {
				result = 0;
				//--------------------------------------
				try {
					console.log('---> Submit Transaction: TestNetwork');
					//--------------------------------------------------
					async function puts(error, stdout, stderr) {
						affichres = stdout + stderr;
						console.log("puts affichres: " + affichres);
						res.render('TestNetwork', { resul: affichres })
						if (error !== null) {
							console.log(`exec error: ${error}`);
						}
						return affichres
					}
					//--------------------------------------------------
					exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./TestNetwork.sh', puts);
					//exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network/ScrpitGUI;./GetDebutOK.sh', puts);
				}
				//--------------------------------------	  
				catch (error) {
					console.error(`******** FAILED TestNetwork: ${error}`);
				}
				//--------------------------------------
			}
		})
		//----------------------------------------------------------------------------------

		app.listen(3005, () => {
			console.log('server app started at port 3005');
		})

		//----------------------------------------------------------------------------------
		printHeure();
		console.log(process.env);
		let param = "";
		var tabparam = new Array();
		let fonction = "";
		console.log(process.argv);
		process.argv.forEach((val, index) => {
			tabparam[index] = val;
			console.log(`${index}: ${val}`);
			if (index == 2) {
				fonction = val;
				param = param + val;
			}
			if (index > 2) {
				param = param + ',';
				param = param + val;
			}
		});
		console.log(" ");
		console.log("param : " + param);
		console.log("fonction : " + fonction);

		if (fonction == "init") {

			// build an in memory object with the network configuration (also known as a connection profile)
			const ccp1 = buildCCPOrg1();
			ccp = ccp1;
			console.log("ccp : " + ccp);

			// build an instance of the fabric ca services client based on
			// the information in the network configuration
			const caClient = buildCAClient(FabricCAServices, ccp, 'ca.org1.example.com');

			// setup the wallet to hold the credentials of the application user
			const wallet1 = await buildWallet(Wallets, walletPath);
			wallet = wallet1;

			// in a real application this would be done on an administrative flow, and only once
			await enrollAdmin(caClient, wallet, mspOrg1);

			// in a real application this would be done only when a new user was required to be added
			// and would be part of an administrative flow
			await registerAndEnrollUser(caClient, wallet, mspOrg1, org1UserId, 'org1.department1');

		}

		try {
			if (fonction == "init") {
				// Create a new gateway instance for interacting with the fabric network.
				// In a real application this would be done as the backend server session is setup for
				// a user that has been verified.
				const gateway = new Gateway();

				// setup the gateway instance
				// The user will now be able to create connections to the fabric network and be able to
				// submit transactions and query. All transactions submitted by this gateway will be
				// signed by this user using the credentials stored in the wallet.
				await gateway.connect(ccp, {
					wallet,
					identity: org1UserId,
					discovery: { enabled: true, asLocalhost: true } // using asLocalhost as this gateway is using a fabric network deployed locally
				});

				// Build a network instance based on the channel where the smart contract is deployed
				const network = await gateway.getNetwork(channelName);

				// Get the contract from the network.
				const contract1 = network.getContract(chaincodeName);
				contract = contract1;

				// Initialize a set of asset data on the channel using the chaincode 'InitLedger' function.
				// This type of transaction would only be run once by an application the first time it was started after it
				// deployed the first time. Any updates to the chaincode deployed later would likely not need to run
				// an "init" type function.


				console.log('<br/><br/>--> Submit Transaction: InitLedger, function creates the initial set of assets on the ledger');
				await contract.submitTransaction('InitLedger');
				console.log("<br/><br/>" + '*** Result: committed');

				// Let's try a query type operation (function).
				// This will be sent to just one peer and the results will be shown.
				console.log('<br/><br/>--> Evaluate Transaction: getAllAssets, function returns all the current assets on the ledger');
				let result = await contract.evaluateTransaction('getAllAssets');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);

				printHeure();
				/*
								exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;ls -alstr', puts);
								exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./TestNetwork.sh', puts);
								exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./ListChannel.sh', puts);
								//			exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./TestLedger.sh 1', puts);	
								exec('cd /home/ope-sboubaker/fabric/fabric-samples/asset-transfer-basic/application-javascript;ls -alstr', puts);
				*/
				printHeure();

			} // FIN init
			//--------------------------------------------------
			//'{"function":"createStock","Args":["1649855219","stockYAM2","100","0"]}'
			if (fonction == "createStock") {
				console.log('<br/><br/>--> Submit Transaction: createStock');
				result = await contract.submitTransaction('createStock', '1649855219', 'stockYAM2', '100', '0');
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>");
					console.log(`*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			if (fonction == "readStock") {
				console.log('<br/><br/>--> Evaluate Transaction: readStock');
				result = await contract.evaluateTransaction('readStock', 'stockYAM2');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			if (fonction == "getAllStocks") {
				console.log('<br/><br/>--> Evaluate Transaction: getAllStocks, function returns all the current Stocks on the ledger');
				result = await contract.evaluateTransaction('getAllStocks');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			//'{"function":"getAssetStock","Args":["stockYAM2"]}'
			if (fonction == "getAssetStock") {
				console.log('<br/><br/>--> Evaluate Transaction: getAssetStock');
				result = await contract.evaluateTransaction('getAssetStock', 'stockYAM2');
				//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);		
				console.log("<br/><br/>" + `*** Result: ${result}`);
			}
			//--------------------------------------------------
			//'{"function":"getAssetEnLivraison","Args":["stockYAM2"]}'
			if (fonction == "getAssetEnLivraison") {
				console.log('<br/><br/>--> Evaluate Transaction: getAssetEnLivraison');
				result = await contract.evaluateTransaction('getAssetEnLivraison', 'stockYAM2');
				//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);		
				console.log("<br/><br/>" + `*** Result: ${result}`);
			}
			//--------------------------------------------------
			//'{"function":"augmentAssetStock","Args":["1649855219","stockYAM2","50"]}'
			if (fonction == "augmentAssetStock") {
				console.log('<br/><br/>--> Submit Transaction: augmentAssetStock');
				result = await contract.submitTransaction('augmentAssetStock', '1649855219', 'stockYAM2', '50');
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			//'{"function":"updateStock","Args":["stockYAM2","200","50"]}'
			if (fonction == "updateStock") {
				console.log('<br/><br/>--> Submit Transaction: updateStock');
				result = await contract.submitTransaction('updateStock', 'stockYAM2', '200', '50');
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			// '{"function":"createContrat","Args":["1649855219","contratYAM1","SANS_COMMANDE","50"]}'
			if (fonction == "createContrat") {
				console.log('<br/><br/>--> Submit Transaction: createContrat');
				result = await contract.submitTransaction('createContrat', "1649855219", 'contratYAM1', 'SANS_COMMANDE', '50');
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			if (fonction == "readContrat") {
				console.log('<br/><br/>--> Evaluate Transaction: readContrat');
				result = await contract.evaluateTransaction('readContrat', 'contratYAM1');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			if (fonction == "getAllContrats") {
				console.log('<br/><br/>--> Evaluate Transaction: getAllContrats, function returns all the current Contrats on the ledger');
				result = await contract.evaluateTransaction('getAllContrats');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			// '{"function":"getContratStatus","Args":["contratYAM1"]}'
			if (fonction == "getContratStatus") {
				console.log('<br/><br/>--> Evaluate Transaction: getContratStatus');
				result = await contract.evaluateTransaction('getContratStatus', 'contratYAM1');
				console.log("<br/><br/>" + `*** Result: ${result}`);
				//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);		
			}
			//--------------------------------------------------
			//'{"function":"getContratQuantity","Args":["contratYAM1"]}'
			if (fonction == "getContratQuantity") {
				console.log('<br/><br/>--> Evaluate Transaction: getContratQuantity');
				result = await contract.evaluateTransaction('getContratQuantity', 'contratYAM1');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			//'{"function":"getAllContratsByStatus","Args":["SANS_COMMANDE"]}'
			if (fonction == "getAllContratsByStatus") {
				console.log('<br/><br/>--> Evaluate Transaction: getAllContratsByStatus');
				result = await contract.evaluateTransaction('getAllContratsByStatus', 'SANS_COMMANDE');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			//'{"function":"getAllContratsByDate","Args":["1649855218","1649855290"]}'
			if (fonction == "getAllContratsByDate") {
				console.log('<br/><br/>--> Evaluate Transaction: getAllContratsByDate');
				result = await contract.evaluateTransaction('getAllContratsByDate', '1649855218', '1649855290');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			//'{"function":"checkAvailability","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
			if (fonction == "checkAvailability") {
				console.log('<br/><br/>--> Submit Transaction: checkAvailability');
				result = await contract.submitTransaction('checkAvailability', "1649855289", 'stockYAM2', "contratYAM1", "30");
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${result}`);
					//				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------					
			//'{"function":"validClient","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
			if (fonction == "validClient") {
				console.log('<br/><br/>--> Submit Transaction: validClient');
				result = await contract.submitTransaction('validClient', "1649855289", 'stockYAM2', "contratYAM1", "30");
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${result}`);
					//				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			//'{"function":"progressDelivery","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
			if (fonction == "progressDelivery") {
				console.log('<br/><br/>--> Submit Transaction: progressDelivery');
				result = await contract.submitTransaction('progressDelivery', "1649855289", 'stockYAM2', "contratYAM1", "30");
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${result}`);
					//				console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			//'{"function":"finalizeDelivery","Args":["1649855289","stockYAM2","contratYAM1","30"]}'
			if (fonction == "finalizeDelivery") {
				console.log('<br/><br/>--> Submit Transaction: finalizeDelivery');
				result = await contract.submitTransaction('finalizeDelivery', "1649855289", 'stockYAM2', "contratYAM1", "30");
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${result}`);
					//				console.log(`*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			//'{"function":"getHistory","Args":["stockYAM2"]}'
			if (fonction == "getHistoryS") {
				console.log('<br/><br/>--> Evaluate Transaction: getHistory STOCK');
				result = await contract.evaluateTransaction('getHistory', 'stockYAM2');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			//'{"function":"getHistory","Args":["contratYAM1"]}'
			if (fonction == "getHistoryC") {
				console.log('<br/><br/>--> Evaluate Transaction: getHistory CONTRAT');
				result = await contract.evaluateTransaction('getHistory', 'contratYAM1');
				console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
			}
			//--------------------------------------------------
			//'{"function":"getEnv","Args":["stockYAM2"]}'
			if (fonction == "getEnvS") {
				console.log('<br/><br/>--> Evaluate Transaction: getEnv STOCK');
				result = await contract.evaluateTransaction('getEnv', 'stockYAM2');
				//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);	
				console.log("<br/><br/>" + '*** Result: committed');
			}
			//--------------------------------------------------
			//'{"function":"getEnv","Args":["contratYAM1"]}'
			if (fonction == "getEnvC") {
				console.log('<br/><br/>--> Evaluate Transaction: getEnv CONTRAT');
				result = await contract.evaluateTransaction('getEnv', 'contratYAM1');
				//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);	
				console.log("<br/><br/>" + '*** Result: committed');
			}
			//--------------------------------------------------
			//--------------------------------------------------
			if (fonction == "deleteStock") {
				console.log('<br/><br/>--> Submit Transaction: deleteStock');
				result = await contract.submitTransaction('deleteStock', 'stockYAM2');
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			if (fonction == "deleteContrat") {
				console.log('<br/><br/>--> Submit Transaction: deleteContrat');
				result = await contract.submitTransaction('deleteContrat', 'contratYAM1');
				console.log("<br/><br/>" + '*** Result: committed');
				if (`${result}` !== '') {
					console.log("<br/><br/>" + `*** Result: ${prettyJSONString(result.toString())}`);
				}
			}
			//--------------------------------------------------
			if (fonction == "init") {
				const serveur = http.createServer((requete, reponse) => {
					console.log(requete.method);
					console.log(requete.url);
					reponse1 = reponse;
					if (nbreq == 0) {
						reponse1.setHeader("content-type", "text/html");
						listeres = listeres + ("<head><meta charset='utf8'><head>");
						//	listeres=listeres+("<h1>SUPPLYCHAIN</h1><p>hello  </p>");
						listeres = listeres + ("<h1>SUPPLY BLOCKCHAIN RING R1.2</h1>");
					}
					nbreq = nbreq + 1;
					console.log('<br/><br/>--> REQUETE : ' + nbreq);
					//result = GAllAssets();

					/*
						listeres = listeres + ('<br/><br/>--> REQUETE : '+nbreq);
						if (nbreq == 1) result = crStock(); 
						if (nbreq == 2) result = lireStock();
						if (nbreq == 3) result = dlStock(); 
						if (nbreq == 4) {
					//		exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;ls -alstr', puts);
							exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./TestNetwork.sh', putw);
					//		exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./ListChannel.sh', putw);
					//		exec('cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./TestLedger.sh 1', putw);
							exec('cd /home/ope-sboubaker/fabric/fabric-samples/asset-transfer-basic/application-javascript', puts);
							affichres="---- SBR R1.0 :  Etat du ORDERER : Up";
							listeres=listeres+('<br/><br/>*** Result SHELL : '+affichres);	
							reponse1.write(listeres);
							reponse1.end();
							console.log("listeres : "+listeres);
					
							printHeure();
							console.log("FIN SHELL ");	
						}
					
					 */
					printHeure();
				});// FIN REQUETE
				//serveur.listen(3001, "localhost",() => {
				//  console.log("Attente des requêtes au port 3001")
				//})
			} // FIN init

			/* yam1 
			
						// Now let's try to submit a transaction.
						// This will be sent to both peers and if both peers endorse the transaction, the endorsed proposal will be sent
						// to the orderer to be committed by each of the peer's to the channel ledger.
						console.log('<br/><br/>--> Submit Transaction: CreateAsset, creates new asset with ID, color, owner, size, and appraisedValue arguments');
						result = await contract.submitTransaction('CreateAsset', 'asset24', 'yellow', '5', 'Tom', '1300');
						console.log("<br/><br/>"+'*** Result: committed');
						if (`${result}` !== '') {
							console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
						}
			
			//			console.log('<br/><br/>--> Evaluate Transaction: ReadAsset, function returns an asset with a given assetID');
			//			result = await contract.evaluateTransaction('ReadAsset', 'asset21');
			//			console.log(`*** Result: ${prettyJSONString(result.toString())}`);
			
						console.log('<br/><br/>--> Evaluate Transaction: AssetExists, function returns "true" if an asset with given assetID exist');
						result = await contract.evaluateTransaction('AssetExists', 'asset1');
						console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			
						console.log('<br/><br/>--> Submit Transaction: UpdateAsset asset1, change the appraisedValue to 350');
						await contract.submitTransaction('UpdateAsset', 'asset1', 'blue', '5', 'Tomoko', '350');
						console.log("<br/><br/>"+'*** Result: committed');
			
						console.log('<br/><br/>--> Evaluate Transaction: ReadAsset, function returns "asset1" attributes');
						result = await contract.evaluateTransaction('ReadAsset', 'asset1');
						console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
						
			///////////////////////////////////////
			//	deleteAsset	
				
						console.log('<br/><br/>--> Submit Transaction: deleteAsset, deletes asset with ID');
						result = await contract.submitTransaction('deleteAsset', 'asset24');
						console.log("<br/><br/>"+'*** Result: committed');
						if (`${result}` !== '') {
							console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
						}	
				
			///////////////////////////////////////
			
			
						try {
							// How about we try a transactions where the executing chaincode throws an error
							// Notice how the submitTransaction will throw an error containing the error thrown by the chaincode
							console.log('<br/><br/>--> Submit Transaction: UpdateAsset asset70, asset70 does not exist and should return an error');
							await contract.submitTransaction('UpdateAsset', 'asset70', 'blue', '5', 'Tomoko', '300');
							console.log("<br/><br/>"+'******** FAILED to return an error');
						} catch (error) {
							console.log("<br/><br/>"+`*** Successfully caught the error: <br/><br/>    ${error}`);
						}
			
						console.log('<br/><br/>--> Submit Transaction: TransferAsset asset1, transfer to new owner of Tom');
						await contract.submitTransaction('TransferAsset', 'asset1', 'Tom');
						console.log("<br/><br/>"+'*** Result: committed');
			
						console.log('<br/><br/>--> Evaluate Transaction: ReadAsset, function returns "asset1" attributes');
						result = await contract.evaluateTransaction('ReadAsset', 'asset1');
						console.log("<br/><br/>"+`*** Result: ${prettyJSONString(result.toString())}`);
			
			 yam1   */

		} finally {
			console.log("<br/><br/>" + "finally..............");
			if (fonction == "disconnect") {
				// Disconnect from the gateway when the application is closing
				// This will close all connections to the network
				console.log("<br/><br/>" + "disconnect gateway..............");
				gateway.disconnect();
			}
		}
	} catch (error) {
		console.error("<br/><br/>" + `******** FAILED to run the application: ${error}`);
	}
}

main();
